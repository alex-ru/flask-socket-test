from app.utils.database import db
from marshmallow import Schema, fields
from datetime import datetime

class UserSchema(Schema):
    user_id = fields.Int(dump_only=True)
    email = fields.Str(required=True)
    password = fields.Str(required=True)
    username = fields.Str(required=True)
    created_at = fields.DateTime(dump_only=True)


class User(db.Model):
    __tablename__ = 'users'
    user_id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.TIMESTAMP, nullable=False)

    def __init__(self, email, password, username):
        self.email = email
        self.password = password
        self.username = username
        self.created_at = datetime.utcnow()
