
from .models import User, UserSchema
from ..utils.database import db

class UserRepository:
    def get_users(self, page=0, per_page=50):
        query = User.query
        pagination = query.paginate(page=page, per_page=per_page, error_out=False)
        user_schema = UserSchema()
        users = pagination.items
        users = [user_schema.dump(user) for user in pagination.items]

        response = {
            'users': users,
            'total_pages': pagination.pages,
            'total_users': pagination.total,
            'has_next': pagination.has_next,
            'has_prev': pagination.has_prev,
            'page': pagination.page,
            'per_page': pagination.per_page
        }

        return response

    def get_user_by_id(self, user_id):
        return User.query.get(user_id)

    def get_user_by_username(self, username):
        return User.query.filter_by(username=username).first()

    def create_user(self, username, password):
        user = User(username=username, password=password)
        db.session.add(user)
        db.session.commit()
        return user

    def update_user(self, user_id, new_data):
        #from app import db
        user = User.query.get(user_id)
        for key, value in new_data.items():
            setattr(user, key, value)
        db.session.commit()
        return user

    def delete_user(self, user_id):
        #from app import db
        user = User.query.get(user_id)
        db.session.delete(user)
        db.session.commit()
