from . import user
# import json
from flask import request, jsonify
from ..user.repository import UserRepository
from ..utils.middlewares import auth_jwt

@user.route('/get', methods=['POST'])
@auth_jwt
def getUsers():
  try:
    body = request.json
    user_repository = UserRepository()
    response = user_repository.get_users(0, 5)
    return jsonify(response)
  except Exception as e:
    error_message = "Error users: {}".format(str(e))
    return jsonify({'error': error_message}), 500
