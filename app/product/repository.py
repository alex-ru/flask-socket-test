
from ..utils.database import db
from .models import Product
from flask import current_app
from sqlalchemy import text

class ProductRepository:
  def get_product_by_id(self, user_id):
    return Product.query.get(user_id)

  def create_product(self, product_new):
    with current_app.app_context():
      product = Product(name=product_new['name'], category=product_new['category'])
      db.session.add(product)
      db.session.commit()
      return product

  def update_product(self, product_id, new_data):
    product = Product.query.get(product_id)
    for key, value in new_data.items():
      setattr(product, key, value)
    db.session.commit()
    return product

  def execute_query_sql(self, sql_query):
    query = text(sql_query)
    db.session.execute(query)
    db.session.commit()
    return 'Sql query executed successfully'

  def delete_user(self, product_id):
    product = Product.query.get(product_id)
    db.session.delete(product)
    db.session.commit()
