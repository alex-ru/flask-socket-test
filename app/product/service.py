from .repository import ProductRepository
from ..utils.socket import SocketManager
import time
from flask_socketio import emit

class ProductService:
  def __init__(self):
    pass

  def import_products(self, products):
    for product in products:
      self.create_product(product)
    return None

  def create_product(self, product):
    socketio = SocketManager.get_instance().socketio
    time.sleep(2)
    socketio.emit('server_message',  product['name'])
    #product_repository = ProductRepository()
    #product_repository.create_product(product)
