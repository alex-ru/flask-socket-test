from . import product
from flask import request, jsonify, render_template
from .service import ProductService
import json

@product.route('/')
def index():
  return render_template('page.html')

@product.route('/import', methods=['POST'])
def import_products():
  try:
    body = request.json
    #
    arr_products_data = body["products"]
    service = ProductService()
    result = service.import_products(arr_products_data)
    print(result)
    return jsonify({'result': 'end'})
  except Exception as e:
    error_message = "Error buying product: {}".format(str(e))
    return jsonify({'error': error_message}), 500