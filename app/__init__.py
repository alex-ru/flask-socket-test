#dependencias
from flask import Flask
from flask_cors import CORS
#configuraciones

from app.config import Config
from app.utils.database import db

#modulos
from app.product import product
from app.auth import auth
from app.user import user

from .utils.socket import SocketManager
from .utils.redis import RedisManager

def create_app():
  app = Flask(__name__)
  app.config.from_object(Config)
  CORS(app)

  #iniciar DB
  db.init_app(app)

  # Inicializar SocketManager
  socketManager = SocketManager(app)
  socketManager.handle_socket()

  # Inicializar Redis
  RedisManager(app)


  #registrar los blueprint (modulos)
  app.register_blueprint(product)
  app.register_blueprint(auth)
  app.register_blueprint(user)

  return app
