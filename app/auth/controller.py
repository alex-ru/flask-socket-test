from . import auth
import json
from flask import request, jsonify
from .services import AuthService
from ..user.repository import UserRepository

@auth.route('/login', methods=['POST'])
def login():
  try:
    body = request.json
    auth_service = AuthService(UserRepository())
    print('body : ', body)
    response = auth_service.login_service(body)
    return jsonify(response)
  except Exception as e:
    error_message = "Error login: {}".format(str(e))
    return jsonify({'error': error_message}), 500
