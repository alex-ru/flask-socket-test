from flask_socketio import SocketIO
from flask import request

def handle_client_message(message, socketio):
    sid = request.sid
    print('Mensaje del cliente:', message, request, 'sid', sid)
    socketio.emit('chat_message', 'Mensaje recibido por el servidor: ' + message)

class SocketManager:
    _instance = None

    def __new__(cls, app=None):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            if app is not None:
                cls._instance._initialize(app)
        return cls._instance

    def _initialize(self, app):
        self.socketio = SocketIO(app, cors_allowed_origins="http://localhost:3000")

    def handle_socket(self):
      socketio = self.get_instance().socketio
      socketio.on('client_message')(lambda message: handle_client_message(message, socketio))
      print("cliente conectado")

    @staticmethod
    def get_instance(app=None):
        if SocketManager._instance is None:
            if app is None:
                raise RuntimeError("SocketManager instance has not been initialized yet.")
            SocketManager._instance = SocketManager(app)
        return SocketManager._instance
