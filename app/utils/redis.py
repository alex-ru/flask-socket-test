import redis
import os

REDIS_PORT = os.environ.get('REDIS_PORT')
REDIS_HOST = os.environ.get('REDIS_HOST')
REDIS_PASSWORD = os.environ.get('REDIS_PASSWORD')

class RedisManager:
    _instance = None

    def __new__(cls, app=None):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            if app is not None:
                cls._instance._initialize(app)
        return cls._instance

    #def _initialize(self, app):
        #self.redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

    def _initialize(self,app=None):
        self.redis_client = redis.StrictRedis(
            host=REDIS_HOST,
            port=REDIS_PORT,
            username="",
            password=REDIS_PASSWORD)

    @staticmethod
    def get_instance(app=None):
        if RedisManager._instance is None:
            if app is None:
                raise RuntimeError("RedisManager instance has not been initialized yet.")
            RedisManager._instance = RedisManager(app)
        return RedisManager._instance
